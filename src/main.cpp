#include <windows.h>

#include "TSFuncs.hpp"

#define Q(x) #x

#define QUOTE(x) Q(x)

#define OVERRIDE_FLT(field, offset) \
    val = tsf_GetDataField(player, "pdf_" QUOTE(field), NULL); \
    float ori_##field = *(float *)(datablock + offset); \
    if (val && *val != '\0') { *(float *)(datablock + offset) = atof(val); };

#define OVERRIDE_S32(field, offset) \
    val = tsf_GetDataField(player, "pdf_" QUOTE(field), NULL); \
    signed int ori_##field = *(signed int *)(datablock + offset); \
    if (val && *val != '\0') { *(signed int *)(datablock + offset) = atoi(val); };

#define REVERT_FLT(field, offset) \
    *(float *)(datablock + offset) = ori_##field;

#define REVERT_S32(field, offset) \
    *(signed int *)(datablock + offset) = ori_##field;

BlFunctionDef(void, __thiscall, Player__updateMove, ADDR, const ADDR *);
BlFunctionHookDef(Player__updateMove);

void __fastcall Player__updateMoveHook(ADDR player, void *blank, const ADDR *move)
{
	if ((*(ADDR *)(player + 68) & 2)) {
		Player__updateMoveOriginal(player, move);
		return;
	}

	const char *val = NULL;
	ADDR datablock = *(ADDR *)(player + 488);

	OVERRIDE_FLT(minLookAngle,      592);
	OVERRIDE_FLT(maxLookAngle,      596);
	OVERRIDE_FLT(runForce,          604);
	OVERRIDE_FLT(runEnergyDrain,    608);
	OVERRIDE_FLT(minRunEnergy,      612);
	OVERRIDE_FLT(airControl,        680);
	OVERRIDE_FLT(jumpForce,         724);
	OVERRIDE_FLT(jumpEnergyDrain,   728);
	OVERRIDE_FLT(minJumpEnergy,     732);
	OVERRIDE_FLT(minJumpSpeed,      736);
	OVERRIDE_FLT(maxJumpSpeed,      740);
	OVERRIDE_S32(jumpDelay,         748);
	OVERRIDE_FLT(jetEnergyDrain,    752);
	OVERRIDE_FLT(minJetEnergy,      756);
	OVERRIDE_S32(canJet,            760);
	OVERRIDE_FLT(horizMaxSpeed,     692);
	OVERRIDE_FLT(horizResistSpeed,  696);
	OVERRIDE_FLT(horizResistFactor, 700);
	OVERRIDE_FLT(upMaxSpeed,        704);
	OVERRIDE_FLT(upResistSpeed,     708);
	OVERRIDE_FLT(upResistFactor,    712);

	Player__updateMoveOriginal(player, move);

	REVERT_FLT(minLookAngle,        592);
	REVERT_FLT(maxLookAngle,        596);
	REVERT_FLT(runForce,            604);
	REVERT_FLT(runEnergyDrain,      608);
	REVERT_FLT(minRunEnergy,        612);
	REVERT_FLT(airControl,          680);
	REVERT_FLT(jumpForce,           724);
	REVERT_FLT(jumpEnergyDrain,     728);
	REVERT_FLT(minJumpEnergy,       732);
	REVERT_FLT(minJumpSpeed,        736);
	REVERT_FLT(maxJumpSpeed,        740);
	REVERT_S32(jumpDelay,           748);
	REVERT_FLT(jetEnergyDrain,      752);
	REVERT_FLT(minJetEnergy,        756);
	REVERT_S32(canJet,              760);
	REVERT_FLT(horizMaxSpeed,       692);
	REVERT_FLT(horizResistSpeed,    696);
	REVERT_FLT(horizResistFactor,   700);
	REVERT_FLT(upMaxSpeed,          704);
	REVERT_FLT(upResistSpeed,       708);
	REVERT_FLT(upResistFactor,      712);
}

void ts_ShapeBase__setRechargeRate(ADDR obj, int argc, const char *argv[])
{
	if (!(*(ADDR *)(obj + 68) & 2))
		*(float *)(obj + 1496) = atof(argv[2]);
}

bool init()
{
	BlInit;

	if (!tsf_InitInternal())
		return false;

	BlScanFunctionHex(Player__updateMove, "55 8B EC 6A FF 68 ? ? ? ? 64 A1 ? ? ? ? 50 81 EC ? ? ? ? A1 ? ? ? ? 33 C5 89 45 F0 56 57 50 8D 45 F4 64 A3 ? ? ? ? 8B F1 89 B5");
	BlCreateHook(Player__updateMove);
	BlTestEnableHook(Player__updateMove);

	tsf_AddConsoleFunc(NULL, "ShapeBase", "setRechargeRate", ts_ShapeBase__setRechargeRate, "(float rate)", 3, 3);

	BlPrintf("%s (v%s-%s): init'd", PROJECT_NAME, PROJECT_VERSION, TSFUNCS_DEBUG ? "debug":"release");
	return true;
}

bool deinit()
{
	BlTestDisableHook(Player__updateMove);

	BlPrintf("%s: deinit'd", PROJECT_NAME);
	return true;
}

bool __stdcall DllMain(HINSTANCE hinstance, unsigned int reason, void *reserved)
{
	switch (reason) {
		case DLL_PROCESS_ATTACH:
			return init();
		case DLL_PROCESS_DETACH:
			return deinit();
		default:
			return true;
	}
}

extern "C" void __declspec(dllexport) __cdecl PROJECT_EXPORT(){}
