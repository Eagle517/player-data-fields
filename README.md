# Player Data Fields
Gives the ability to apply per-player fields that are otherwise bound to a player datablock.

## Usage
Use the latest version of [RedBlocklandLoader](https://gitlab.com/Eagle517/redblocklandloader/-/releases) to easily load the DLL into the game.

The following fields are able to be applied to a player object (make sure to add `pdf_` before each field):
```
minLookAngle
maxLookAngle
runForce
runEnergyDrain
minRunEnergy
airControl
jumpForce
jumpEnergyDrain
minJumpEnergy
minJumpSpeed
maxJumpSpeed
jumpDelay
jetEnergyDrain
minJetEnergy
canJet
horizMaxSpeed
horizResistSpeed
horizResistFactor
upMaxSpeed
upResistSpeed
upResistFactor
```
For example, set `%player.pdf_canJet = false;` to override their datablock's `canJet` field.  Set the player field to an empty string to return to normal behavior (e.g. `%player.pdf_canJet = "";`).

With v1.1.0, this DLL also adds back `ShapeBase::setRechargeRate(float rate)` which was removed from the game a while ago.  This will let you control individual player's recharge rates by doing, for example, `%player.setRechargeRate(0.1);`.

## Issues
The player fields are in no way networked to the client.  This will cause minor desync on the client's end in the sense that animations and predictions may not be completely correct.  However, it's very minor and can be overlooked in most cases.  The main intention behind this DLL is to decrease the amount of ghost updates that occur by changing players' datablocks during gameplay.

## Building
This DLL relies on [TSFuncs](https://gitlab.com/Eagle517/tsfuncs) which you will need to build first.  Building with CMake is straightforward, although it has only been setup for MinGW.  A toolchain has been included for cross-compiling on Linux.
